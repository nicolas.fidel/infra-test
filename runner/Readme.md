## Gitlab runner

In this folder there are helm files to deploy gitlab runners

https://docs.gitlab.com/runner/install/kubernetes.html

## Helm file :

Helm files are templates that allow
you to easily deploy applications in kubernetes

Gitlab helm file documentation: 
https://docs.gitlab.com/runner/install/kubernetes.html

Good tutorial for helm:
https://www.youtube.com/watch?v=s2Q7pHIGjRM&list=PLn6POgpklwWqIeLqU1194K-LS1O5m2lDd

## Problem with gitlab.com
You can't deploy a global runner,
but you have to deploy one runner per project

## How to deploy a runner
specify token and link to gitlab in helm file

```bash
    # Basic alias to simplify cli
    alias h="helm --kubeconfig <path_to_config>"

    # Add chart
    h repo add gitlab.com https://charts.gitlab.io

    # Install
    h install --namespace <namespace> --name <gitlab-runner-name> -f <helm-file> gitlab/gitlab-runner
```

## Upgrading runner config

```bash 
    h upgrade --namespace <namespace> -f <helm-file> <gitlab-runner-name> gitlab/gitlab-runner
```
