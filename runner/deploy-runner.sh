alias k=kubectl\ --kubeconfig\ ~/prod-yaka.yaml
alias h="helm --kubeconfig ~/prod-yaka.yaml"

h install --namespace gitlab-runner gitlab-runner-terraform -f runner-terraform-project.yaml gitlab/gitlab-runner
h install --namespace gitlab-runner gitlab-runner-autodoc -f runner-autodoc-project.yaml gitlab/gitlab-runner
h install --namespace gitlab-runner gitlab-runner-myfaas -f runner-myfaas-project.yaml gitlab/gitlab-runner
h install --namespace gitlab-runner gitlab-runner-infra -f runner-infra-project.yaml gitlab/gitlab-runner

h list
